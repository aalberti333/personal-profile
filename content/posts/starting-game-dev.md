---
title: "Starting Game Dev"
date: 2021-12-19T09:02:59-07:00
draft: false
---

## Learning video game development from the beginning

I wanted to write this up before forgetting too many things from the beginner's point of view. I am definitely still a beginner when it comes to game development, put this is always something I wish I'd done when I started software development a few years ago. There's this idea that as somebody grows experience in their field, they tend to think about problems differently. Ideas that were once difficult to reason about become second nature, and as people progress in their skillsets, they start to lose touch with what concepts they struggled with in the beginning of their journey and the explanations that helped them reach their current understanding. So, seeing as I'm just taking my first steps in game development, I'll be writing down the path I took to get to many of my "oh, that's how that works!" moments in this space.

Probably makes sense to start with a few disclaimers at this point. When I started learning game dev about a year ago, I had about 3 years of software development experience in the private sector. Programming was already familiar to me, and I had some sense of which tools existed in this space to help me get up and running. My development is also from a hobbyist standpoint. I have a fulltime position as a software dev (not in gaming) that supports my family and I, which factors a bit into the time I can spending developing, but moreso in that my livelihood doesn't depend on getting a job at a big game development studio or creating a hit indie game to survive.

Okay cool, with that out of the way, here's out it started.

## In the beginning there was Discord (John 1:1, kinda)
