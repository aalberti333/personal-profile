---
title: ABOUT
description: Hey, I'm Anthony (Tony) Alberti
images: ["/images/sample.jpg"]
---


Hey there! :wave:

I'm Tony, a software developer with a passion for exploring fringe and bleeding-edge technologies. My current interests lie in web3, XR, game dev, and modern languages.

Most of my hobby time is spent exploring advances in the blockchain ecosystems, getting my hands dirty in the Godot game engine, and writing proof-of-concept applications in whatever language I'm obsessing over at the time.